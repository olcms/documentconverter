#!/usr/bin/env bash

set -euo pipefail

# get IP of the Docker host
export DOCKER_HOST_IP="$(/sbin/ip route|awk '/default/ { print $3 }')"

# install dependencies
pip install -r requirements/dev.txt \
    --user --upgrade --cache-dir ${PIPCACHE_DIR}

export PATH=${PYTHONUSERBASE}/bin:${PATH}

exec ${@}
