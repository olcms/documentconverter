#!/usr/bin/env bash

set -euo pipefail

apt-get update

apt-get install -yq \
            pandoc \
            texlive-latex-base \
            texlive-fonts-recommended

apt-get clean
rm -rf /var/apt/cache
rm -rf /var/tmp/*
rm -rf /tmp/*
