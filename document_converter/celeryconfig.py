# -*- coding: utf-8 -*-
import os

from kombu import Queue, Exchange


broker_url = os.environ.get('CELERY_BROKER_URL')

result_backend = broker_url

imports = ('document_converter.tasks',)

worker_queue = os.environ.get('WORKER_QUEUE', 'default')

task_queues = (Queue(worker_queue, Exchange(worker_queue), worker_queue),)

task_routes = {
    'document_converter.*': worker_queue,
}
