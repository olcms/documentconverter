# -*- coding: utf-8 -*-
import base64
import os
import tempfile

import pypandoc


def convert(
    document: str,
    input_format: str,
    output_format: str,
    outputfile: str=None,
    extra_args: list = None
):
    if extra_args is None:
        extra_args = []

    return pypandoc.convert_text(
        document,
        format=input_format,
        to=output_format,
        outputfile=outputfile,
        extra_args=extra_args
    )


def convert_via_file(document: str, input_format: str, output_format: str):
    output_file, output_file_path = tempfile.mkstemp(suffix=".%s" % output_format)
    convert(document, input_format=input_format, output_format=output_format, outputfile=output_file_path)
    with open(output_file_path, 'rb') as output_file:
        result = output_file.read()
    os.remove(output_file_path)
    return result


def convert_md_to_html(document: str):
    return convert(
        document,
        input_format='md',
        output_format='html',
        extra_args=['--standalone']
    )


def convert_md_to_pdf(document: str):
    return convert_via_file(document, input_format='md', output_format='pdf')


def convert_md_to_odt(document: str):
    return convert_via_file(document, input_format='md', output_format='odt')


def convert_md_to_docx(document: str):
    return convert_via_file(document, input_format='md', output_format='docx')
