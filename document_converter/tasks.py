import base64

from .celery import app
from . import converter


@app.task
def convert_md_to_html(document: str):
    result = converter.convert_md_to_html(document)
    return format_text(result)


@app.task
def convert_md_to_pdf(document: str):
    result = converter.convert_md_to_pdf(document)
    return format_base64(result)


@app.task
def convert_md_to_odt(document: str):
    result = converter.convert_md_to_odt(document)
    return format_base64(result)


@app.task
def convert_md_to_docx(document: str):
    result = converter.convert_md_to_docx(document)
    return format_base64(result)


def format_text(data: str):
    return {
        'result': data,
        'format': 'text'
    }


def format_base64(data: bytes):
    result = base64.b64encode(data).decode('ascii')
    return {
        'result': result,
        'format': 'base64'
    }
