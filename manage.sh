#!/usr/bin/env bash

set -eo pipefail

BASE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
COMPOSE_DIR="${BASE_DIR}/docker/dev"

export UID
export GID=$(id --group)

DC="docker-compose -f ${COMPOSE_DIR}/docker-compose.yml"

_usage() {
cat << EOF
### Docker Compose helper for document_converter project
### usage: manage.sh COMMAND

Available CONTAINERS:
- converter
- redis

COMMAND:
# PROJECT RELATED
    build-dev       Build locally all developer images declared in docker-compose file
    build-prod      Build locally production image
    make            Run make command inside converter container
    monitor         Monitoring celery app via flower

# DOCKER RELATED
    up              Create and start containers (will stop and recreate containers if configuration or image has changed)
    down            Stop and remove containers, networks, images, and volumes
    stop            Stop running containers without removing them. They can be started again with "manage.sh start"
    start           Start existing containers (after they were stopped)
    restart         Restart containers
    ps              List containers
    run             Run a one-off command (e.g. "manage.sh run CONTAINER less /etc/hosts")
    logs            View combined logs from all containers (use "manage.sh logs -f" to follow output)
    logs CONTAINER  View specified container logs (use "manage.sh logs -f CONTAINER" to follow output)
    shell CONTAINER Log into a bash session inside a running container

EOF
}

case "$1" in
    "build-dev")
        echo "Building developer images "${@:2}"... (CTRL+C to stop)"
        ${DC} build --pull ${@:2}
        ;;

    "build-prod")
        echo "Building production image... (CTRL+C to stop)"
        docker build -t olcms/documentconverter --pull ${BASE_DIR}
        ;;

    "make")
        echo "Running make "${@:2}
        ${DC} run --rm converter make ${@:2}
        ;;

    "monitor")
        echo "Running flower for celery monitoring"
        ${DC} exec converter make monitor
        ;;

    "up")
        echo "Starting containers "${@:2}" in the background..."
        ${DC} up -d ${@:2}
        ;;

    "stop")
        echo "Stopping containers "${@:2}"..."
        ${DC} stop ${@:2}
        ;;

    "start")
        echo "Starting containers "${@:2}"..."
        ${DC} start ${@:2}
        ;;

    "restart")
        echo "Restarting containers "${@:2}"..."
        ${DC} restart
        ;;

    "rm")
        echo "Stopping and removing containers "${@:2}"..."
        ${DC} stop ${@:2}
        ${DC} rm -f ${@:2}
        ;;

    "ps")
        echo "Listing containers..."
        ${DC} ps
        ;;

    "shell")
        DOCKER_SHELL_CONTAINER=$(${DC} ps | grep ${@:2} | cut -d ' ' -f 1 | head -n 1)
        echo "Logging into container ${DOCKER_SHELL_CONTAINER}..."
        docker exec -it ${DOCKER_SHELL_CONTAINER} bash
        ;;

    "logs")
        echo "Accessing logs "${@:2}"..."
        ${DC} logs ${@:2}
        ;;

    "run")
        echo "Running command '"${@:3}"' in "${2}"..."
        ${DC} run --rm ${@:2}
        ;;

    "down")
        echo "Destroying all containers and volumes..."
        ${DC} down -v
        ;;

    *)
        _usage
        exit 0
        ;;
esac
