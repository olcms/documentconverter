===============================
Document Converter
===============================


.. image:: https://img.shields.io/pypi/v/document_converter.svg
        :target: https://pypi.python.org/pypi/document_converter

.. image:: https://img.shields.io/travis/mikus/document_converter.svg
        :target: https://travis-ci.org/mikus/document_converter

.. image:: https://readthedocs.org/projects/document-converter/badge/?version=latest
        :target: https://document-converter.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status

.. image:: https://pyup.io/repos/github/mikus/document_converter/shield.svg
     :target: https://pyup.io/repos/github/mikus/document_converter/
     :alt: Updates


OLCMS document converter


* Free software: Apache Software License 2.0
* Documentation: https://document-converter.readthedocs.io.


Features
--------

* TODO

Credits
---------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage

